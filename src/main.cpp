/*
    SPDX-FileCopyrightText: 2022 Aleix Pol Gonzalez <aleixpol@kde.org>
    SPDX-FileCopyrightText: 2022 Ilya Fedin <fedin-ilja2010@ya.ru>

    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
*/

#include <QDBusConnection>
#include <QDBusMessage>
#include <QWaylandClientExtensionTemplate>
#include <QtWidgets>
#include <qpa/qplatformnativeinterface.h>

#include "qwayland-xdg-activation-v1.h"

class WaylandXdgActivationTokenV1 : public QObject, public QtWayland::xdg_activation_token_v1
{
    Q_OBJECT

public:
    explicit WaylandXdgActivationTokenV1(::xdg_activation_token_v1 *handle)
        : QtWayland::xdg_activation_token_v1(handle)
    {
    }

    ~WaylandXdgActivationTokenV1() override
    {
        destroy();
    }

    void xdg_activation_token_v1_done(const QString &token) override
    {
        Q_EMIT done(token);
    }

Q_SIGNALS:
    void done(const QString &token);
};

class WaylandXdgActivationV1 : public QWaylandClientExtensionTemplate<WaylandXdgActivationV1>, public QtWayland::xdg_activation_v1
{
public:
    WaylandXdgActivationV1()
        : QWaylandClientExtensionTemplate<WaylandXdgActivationV1>(1)
    {
        QMetaObject::invokeMethod(this, "addRegistryListener");
    }

    WaylandXdgActivationTokenV1 *requestToken(wl_seat *seat, struct ::wl_surface *surface, uint32_t serial, const QString &app_id)
    {
        auto token = new WaylandXdgActivationTokenV1(get_activation_token());

        if (surface) {
            token->set_surface(surface);
        }
        if (!app_id.isEmpty()) {
            token->set_app_id(app_id);
        }
        if (seat) {
            token->set_serial(serial, seat);
        }

        token->commit();
        return token;
    }
};

static QString objectPathFromServiceName(const QString &serviceName)
{
    QString objectPath = serviceName;
    objectPath.replace(QLatin1Char('.'), QLatin1Char('/'));
    objectPath.replace(QLatin1Char('-'), QLatin1Char('_'));
    return QLatin1Char('/') + objectPath;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow()
    {
        setWindowTitle(QStringLiteral("dbus xdg-activation helper"));

        QVBoxLayout *mainLayout = new QVBoxLayout();
        setLayout(mainLayout);

        QFormLayout *formLayout = new QFormLayout();
        mainLayout->addLayout(formLayout);

        QLabel *serviceName = new QLabel(QStringLiteral("Service name:"));
        m_serviceNameEdit = new QLineEdit();
        formLayout->addRow(serviceName, m_serviceNameEdit);

        QPushButton *activateButton = new QPushButton(QStringLiteral("Activate"));
        connect(activateButton, &QPushButton::clicked, this, &MainWindow::handleActivateClicked);
        mainLayout->addWidget(activateButton);
    }

private:
    void handleActivateClicked()
    {
        QPlatformNativeInterface *native = qGuiApp->platformNativeInterface();
        auto seat = static_cast<wl_seat *>(native->nativeResourceForIntegration("wl_seat"));
        wl_surface *surface = reinterpret_cast<wl_surface *>(native->nativeResourceForWindow(QByteArrayLiteral("surface"), windowHandle()));
        auto req = m_activation.requestToken(seat, surface, 0, {});
        connect(req, &WaylandXdgActivationTokenV1::done, this, [this](const QString &token) {
            const QString serviceName = m_serviceNameEdit->text();
            const QString objectPath = objectPathFromServiceName(serviceName);

            auto message = QDBusMessage::createMethodCall(serviceName,
                                                          objectPath,
                                                          QStringLiteral("org.freedesktop.Application"),
                                                          QStringLiteral("Activate"));

            const QVariantMap parameterData {
                {QStringLiteral("activation-token"), token},
            };

            qDebug() << "parameter-data" << parameterData;
            message.setArguments({parameterData});

            const QDBusMessage reply = QDBusConnection::sessionBus().call(message);
            if (!reply.errorMessage().isEmpty()) {
                qDebug() << reply.errorMessage();
            }
        });
    }

    WaylandXdgActivationV1 m_activation;
    QLineEdit *m_serviceNameEdit;
};

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainWindow w;
    w.show();

    return app.exec();
}

#include "main.moc"
